void main() {
  print("Looping Pertama");
  var loopingpertama = 2;
  while (loopingpertama <= 20) {
    if (loopingpertama % 2 == 0) {
      print(" $loopingpertama - I love coding");
    }
    loopingpertama++;
  }

  print("");

  print("Looping Kedua");
  var loopingkedua = 20;
  while (loopingkedua >= 2) {
    if (loopingkedua % 2 == 0) {
      print(" $loopingkedua - I will become a mobile developer");
    }
    loopingkedua--;
  }
}
