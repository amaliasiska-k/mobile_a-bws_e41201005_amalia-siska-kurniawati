import 'dart:io';

void main() {
  print("Game Werewolf");
  stdout.write("Input nama player : ");
  String name = stdin.readLineSync()!;

  if (name == "") {
    print("Nama player tidak boleh kosong!");
  } else {
    var firstrole = "Penyihir", secondrole = "Guard", thirdrole = "Werewolf";

    stdout.write("Pilih role player ($firstrole, $secondrole, $thirdrole) : ");
    String player = stdin.readLineSync()!;

    if (player == "") {
      print("Hallo $name, pilih role karaktermu untuk memulai game!");
    } else if (player == firstrole) {
      print(
          "Halo $player $name, kamu dapat melihat siapa yang menjadi werewolf");
    } else if (player == secondrole) {
      print(
          "Halo $player $name, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if (player == thirdrole) {
      print("Halo $player $name, kamu akan memakan mangsa setiap malam!");
    } else {
      print(
          "Ups, role player tidak ditemukan!");
    }
  }
}
